#!/usr/bin/env python
# -*- coding: utf-8 -*-

# [Arkos Tracker 2 to SNDH converter]

strVersion="0.19"
strProgramHeader="---[ aks2sndh.py v"+strVersion+" ]------------------------------------------------"
strProgramDescription="[ Arkos Tracker 2 to SNDH converter ]"

"""

===============================================================================================
===============================================================================================
== ToDo



== ToDo
===============================================================================================
===============================================================================================
== Done



== Done
===============================================================================================
===============================================================================================
== Thoughts



== Thoughts
===============================================================================================
===============================================================================================

"""

boolAllowScreenOutput=True

if boolAllowScreenOutput: print strProgramHeader

boolDebug=True

boolDebugPrinting=False


###############################################################################################
###############################################################################################
## Defines and constants


NO_ERROR=0
numErrBit=1
FILE_EXISTS_FAIL=numErrBit
numErrBit*=2
FILE_READABLE_FAIL=numErrBit
numErrBit*=2


## Defines and constants
###############################################################################################
###############################################################################################
## Subroutines


#------------------------------------------------------------------------------


def CheckFileExistsAndReadable(strFile):
  numOut=0
  boolHit=False
  if os.path.exists(strFile) and os.path.isfile(strFile):
    boolHit=True
  if not boolHit:
    numOut+=FILE_EXISTS_FAIL
  boolHit=False
  if os.access(strFile, os.R_OK):
    boolHit=True
  if not boolHit:
    numOut+=FILE_READABLE_FAIL
  return numOut


def GetBinaryFile(strFile):
  # Returns contents of binary file in array of ints, where each int represents a byte
  arrFile=[]
  byteThis=open(strFile, "rb").read()
  for b in byteThis:
    arrFile.append(ord(b))
  return arrFile


def GetTextFile(strFile):
  # Return contents of text file in array of strings, where each string is a line in the text file
  arrFile=open(strFile, "r").readlines()
  return arrFile


def WriteBinaryFile(arrData, strFile):
  # Writes binary data from arrData to binary file
  # Binary data is expected as an array of ints, where each int represents a byte
  fileOut=open(strFile, "wb")
  arrByteData=bytearray(arrData)
  fileOut.write(arrByteData)
  fileOut.close()


def WriteTextFile(arrData, strFile):
  # Writes data from arrData to textfile
  # Data is expected as an array of strings, where each string represents a line of text
  fileOut=open(strFile, "w")
  for strThis in arrData:
    fileOut.write(strThis+"\n")
  fileOut.close()


#------------------------------------------------------------------------------


def Out(str):
  global boolAllowScreenOutput
  if boolAllowScreenOutput:
    sys.stdout.write(str)


#------------------------------------------------------------------------------


def Fatal(str):
  # Fatal errors always output
  boolAllowScreenOutput=True
  Out("\nFATAL ERROR: "+str+"\n")
  exit(1)


def Warn(str):
  #boolAllowScreenOutput=True
  Out("WARNING: "+str+"\n")


def Notice(str):
  #boolAllowScreenOutput=True
  Out("NOTICE: "+str+"\n")


#------------------------------------------------------------------------------


def Exe(arrThis):
  return subprocess.check_output(arrThis)


def ExeWithOutput(arrExe):
  process = subprocess.Popen(
      arrExe, stdout=subprocess.PIPE, stderr=subprocess.PIPE
  )
  while True:
    out = process.stdout.read(1)
    if out == '' and process.poll() != None:
      break
    if out != '':
      if out=="\n":
        sys.stdout.write("\n")
        sys.stdout.flush()
      elif out=="\r":
        sys.stdout.write("\r")
        sys.stdout.flush()
      else:
        sys.stdout.write(out)
        #sys.stdout.flush()
  sys.stdout.write("\n")
  sys.stdout.flush()


#------------------------------------------------------------------------------


def StrToBool(n):
  if n=="True":
    return True
  return False


#------------------------------------------------------------------------------


ZXRandSeed=3.14159265358979

def ZXRand():
  # Lehmer random number generator, this is the algorithm used by Sinclair in the ZX Spectrum Basic
  # http://en.wikipedia.org/wiki/Lehmer_random_number_generator
  # Requires an int in global scope named $ZXRandSeed - set this to a unique value per project
  # Returns a number between 0 and 1, will NEVER reach 1 but sometimes 0
  global ZXRandSeed
  ZXRandSeed=((int)(((ZXRandSeed+1)*75)-1)%65537.0)
  return ZXRandSeed/65537.0


def ZXRandSpan(numMin,numMax):
  # Returns a random number between numMin and numMax, with a slight weighting FROM $numMax
  # Expects ints, but should work ok with decimals too
  boolDebug=False
  if(numMax<numMin):
    numTemp=numMin
    numMin=numMax
    numMax=numTemp
  if boolDebug:
    print "--ZXRandSpan("+str(numMin)+","+str(numMax)+")="
  numSpan=numMax-numMin
  return numMin+(ZXRand()*numSpan)


#------------------------------------------------------------------------------


numTimingClock=0
def TimingStart():
  global numTimingClock
  numTimingClock=time.clock()


def GetTimingEnd():
  return time.clock()-numTimingClock


#------------------------------------------------------------------------------


def CorrectNeg(numVal, numBits):
  numOut=0
  if numVal>=0:
    numOut=numVal
  else:
    numVal=abs(numVal)
    strBin=bin(numVal)[2:].zfill(numBits)
    #print strBin
    strComp=""
    for thisBit in strBin:
      if thisBit=="0":
        strComp+="1"
      else:
        strComp+="0"
    numComp=int(strComp,2)
    numComp+=1
    #print bin(numComp)[2:].zfill(32)
    numOut=numComp
  return numOut


def MakeDevpacWord(n):
  n=CorrectNeg(n, 16)
  strN=hex(n)
  strN=strN.lstrip("0x")
  strN=strN.rstrip("L")
  strN=strN.zfill(4)
#  while(len(strN)<3):
#    strN="0"+strN
  return "$"+strN
  

def MakeDevpacLongword(n):
  n=CorrectNeg(n, 32)
  strN=hex(n)
  strN=strN.lstrip("0x")
  strN=strN.rstrip("L")
  strN=strN.zfill(8)
#  while(len(strN)<8):
#    strN="0"+strN
  return "$"+strN


#------------------------------------------------------------------------------


def GetStringNybble(n):
  strNybble=str(bin(n))
  strNybble=strNybble.lstrip("0b")
  while(len(strNybble)<4):
    strNybble="0"+strNybble
  return strNybble


def GetStringByte(n):
  strByte=str(bin(n))
  strByte=strByte.lstrip("0b").zfill(8)
  #while(len(strByte)<8):
  #  strByte="0"+strByte
  return strByte


def ToSignedWord(val):
  return val%65536


def FromSignedWord(val):
  if val>32767:
    return -(65536-val)
  return val



#------------------------------------------------------------------------------



## Subroutines
###############################################################################################
###############################################################################################

import sys
import os
import time
import copy
import subprocess
import argparse

#print "\nHow to use: Edit this file.\nTo find the variables you want to edit, search for the text \"edit here\".\n"

###############################################################################################
###############################################################################################
###############################################################################################
###############################################################################################
###############################################################################################
### Edit here
#
#
## Input filename, i e the .aks file from Arkos Tracker 2
#strInputFilename="knightmare.aks"
#
## Output filename, i e the end result
#strSndhFilename="knightmare.sndh"
#
## Title
#strSndhTitle="Remote entry #2"
## Composer
#strSndhComposedBy="Excellence In Art"
## Year
#strSndhYear="2018"
## Timer(s) to use - normally this is just the value 50
#numSndhTimerC=200
## Play time
#numSndhTimeInSeconds=120
#
## Ripped by
#strSndhRippedBy=""
## Converted to SNDH by
#strSndhConvertedBy="Excellence In Art"
#
#
### 
###############################################################################################
###############################################################################################
###############################################################################################
###############################################################################################
###############################################################################################

objParser = argparse.ArgumentParser(description=strProgramDescription)

objParser.add_argument("-i", "--infile",
                       dest="infile",
                       help="AKS file",
                       metavar="FILE",
                       required=True)
objParser.add_argument("-o", "--outfile",
                       dest="outfile",
                       help="SNDH filename",
                       metavar="FILE",
                       required=True)
objParser.add_argument("-v", "--verbose",
                       dest="verbose",
                       help="shows additional information during processing (default is off)",
                       action="store_const",
                       const=True,
                       required=False,
                       default=False)
objParser.add_argument("-q", "--quiet",
                       dest="quiet",
                       help="no output during processing, except errors (default is off)",
                       action="store_const",
                       const=True,
                       required=False,
                       default=False)


# Parameter defaults
boolInFileGiven=False
strInFile=""
boolOutFileGiven=False
strOutFile=""
boolVerboseMode=False
boolQuietMode=False

numArgs=0
Args=vars(objParser.parse_args())

for key in Args:
  numArgs+=1
  strValue=str(Args[key])
  #print str(key)+" = "+str(strValue)
  if key=="infile" and strValue!="None":
    boolInFileGiven=True
    strInFile=strValue
  elif key=="outfile" and strValue!="None":
    boolOutFileGiven=True
    strOutFile=strValue
  elif key=="verbose" and strValue!="None":
    boolVerboseMode=StrToBool(strValue)
  elif key=="quiet" and strValue!="None":
    boolQuietMode=StrToBool(strValue)

# No args = print usage and exit
if numArgs==0:
  objParser.print_help()
  exit(0)


# Adjust combinations of verbose and quiet mode
if (boolVerboseMode & boolQuietMode):
  Out("Notice: Both verbose and quiet mode sent as parameters, verbose mode chosen.\n\n")
  boolQuietMode=False


###############################################################################
## Test: Unzip .aks file

import zipfile
from StringIO import StringIO
import xml.etree.ElementTree as et

myZipFile=zipfile.ZipFile(strInFile)
# An .AKS should only contain a single file, so let's fetch that uglily
for thisEntry in myZipFile.infolist():
  strXmlFilename=thisEntry.filename

myZipData=myZipFile.read(strXmlFilename) 

#print "len(myZipData): "+str(len(myZipData))

#xRoot=et.fromstring(myZipData)

# Strip out namespaces, thanks to https://stackoverflow.com/questions/13412496/python-elementtree-module-how-to-ignore-the-namespace-of-xml-files-to-locate-ma
it = et.iterparse(StringIO(myZipData))
for _, el in it:
    if '}' in el.tag:
        el.tag = el.tag.split('}', 1)[1]  # strip all namespaces
xRoot = it.root

strSndhComposedBy=xRoot.findall("author")[0].text
strSndhTitle=xRoot.findall("title")[0].text
strSndhYear=xRoot.findall("modificationDate")[0].text[:4]
subsongs=xRoot.findall("subsongs")[0]
numReplayFreq=int(subsongs[0].findall("replayFrequency")[0].text)
numInitialSpeed=int(subsongs[0].findall("initialSpeed")[0].text)
numSongEnd=int(subsongs[0].findall("endIndex")[0].text)
psg=subsongs[0].findall("psgMetadata")
strYmFreq=str(psg[0].findall("psgFrequency")[0].text)

if strYmFreq!="2000000":
  Fatal("An SNDH file requires that the sound chip is clocked at 2MHz.\nIn Arkos Tracker 2:\n  Edit --> Song properties --> PSG list")

strSndhRippedBy=""
strSndhConvertedBy="aks2sndh v"+strVersion+" by XiA"


if boolVerboseMode:
  print
  print "TITL: "+strSndhTitle
  print "COMM: "+strSndhComposedBy
  print "YEAR: "+strSndhYear
  print "CONV: "+strSndhConvertedBy
  print "Clock: TC"+str(numReplayFreq)
  print "YM freq: "+strYmFreq
  #print "Initial speed: "+str(numInitialSpeed)

#print "-"*50

# Build temporary speedtracks
speedtracks=subsongs[0].findall("speedTracks")
arrSpeedTracks=[]
for thisSpeedTrack in speedtracks[0]:
  newSpeedTrack={}
  numNumber=int(thisSpeedTrack.findall("number")[0].text)
  newSpeedTrack["number"]=numNumber
  #print "  SpeedTrack "+str(numNumber)+":"
  speedcells=thisSpeedTrack.findall("speedCell")
  arrSpeedCells=[]
  for thisSpeedCell in speedcells:
    newSpeedCell={}
    numPos=int(thisSpeedCell[0].text)
    numVal=int(thisSpeedCell[1].text)
    newSpeedCell["pos"]=numPos
    newSpeedCell["value"]=numVal
    arrSpeedCells.append(newSpeedCell)
  newSpeedTrack["cells"]=arrSpeedCells
  arrSpeedTracks.append(newSpeedTrack)

arrTemp=copy.deepcopy(arrSpeedTracks)
arrSpeedTracks=[]
# Fill with blanks
# Get highest number
numHigh=-1
for thisOne in arrTemp:
  if thisOne["number"]>numHigh:
    numHigh=thisOne["number"]
#print "numHigh: "+str(numHigh)
# Fill with dummy data
numTracks=numHigh+1
for i in range(numTracks):
  arrSpeedTracks.append([])

for thisOne in arrTemp:
  numNumber=thisOne["number"]
  arrCells=thisOne["cells"]
  arrPattern=[-1]*256
  #print "numNumber: "+str(numNumber)
  #print "cells: "+str(arrCells)
  #print "arrPattern: "+str(arrPattern)
  # Fill pattern
  for thisCell in arrCells:
    numPos=thisCell["pos"]
    numVal=thisCell["value"]
    arrPattern[numPos]=numVal
  #print "arrPattern: "+str(arrPattern)
  arrSpeedTracks[numNumber]=arrPattern

print 
numSpeed=numInitialSpeed
numTicks=0

patterns=subsongs[0].findall("patterns")
for n in range(numSongEnd):
  thisPattern=patterns[0][n]
#for thisPattern in patterns[0]:
  numHeight=int(thisPattern.findall("height")[0].text)
  numSpeedTrack=int(thisPattern.findall("speedTrackNumber")[0].text)
  #print "numHeight: "+str(numHeight)+", "+"numSpeedTrack: "+str(numSpeedTrack)
  for i in range(numHeight):
    thisSpeed=arrSpeedTracks[numSpeedTrack][i]
    if thisSpeed>=0:
      numSpeed=thisSpeed
    numTicks+=numSpeed
    
numSndhTimeInSeconds=numTicks/numReplayFreq

if boolVerboseMode:
  print "Ticks: "+str(numTicks)
  print "Seconds: "+str(numSndhTimeInSeconds)
  print
#print "numSongEnd: "+str(numSongEnd)

#for thisOne in xRoot.findall("author"):
#  #print str(thisOne)
#  #print dir(thisOne)
#  print thisOne.text
##  print "Tag: "+str(thisChild.tag)
##  print "Attrib: "+str(thisChild.attrib)
##  print

#exit(0)

## Test: Unzip .aks file
###############################################################################


def RmacSlash(strThis):
  return strThis.replace("\\", "/")


def KillFile(strFile):
  if os.path.isfile(strFile):
    os.remove(strFile)


def TestFile(strFile):
  numResult=CheckFileExistsAndReadable(strFile)
  if numResult!=0:
    if numResult&FILE_EXISTS_FAIL:
      Fatal("Can't find file \""+strFile+"\"!")
    if numResultFILE_READABLE_FAIL:
      Fatal("Can't read file \""+strFile+"\"!")


def Write(arrThis, strFilename):
  if not boolQuietMode:
    Out("Writing \""+strFilename+"\"... ")
  WriteTextFile(arrThis, strFilename)
  if not boolQuietMode:
    Out("done! "+str(len(arrThis))+" lines written.\n")


TestFile(strInFile)

#strInputFileStub=strInputFilename[:-4]

strSongFile="src"+os.sep+"song.aky.s"
strEventsFile="src"+os.sep+"song.events.words.s"

#------------------------------------------------------
#-- This used to be conv_aks.bat


ExeWithOutput(["bin"+os.sep+"SongToAky", "-adr", "0", "-spadr", ";", "--sourceProfile", "68000", "-sppostlbl", ":", "-reladr", "-spomt", strInFile, strSongFile])
TestFile(strSongFile)

ExeWithOutput(["bin"+os.sep+"SongToEvents", "-adr", "0", "-spadr", ";", "--sourceProfile", "68000", "-sppostlbl", ":", "-spomt", strInFile, strEventsFile])
TestFile(strEventsFile)

#ExeWithOutput(["bin"+os.sep+"sed", "-i", "-e", "\"s/dc\\.b/dc.w/gI\"", "-e", "\"s/dc\\.w Events_/dc.l Events_/gI\"", strEventsFile])
arrEventsFile=GetTextFile(strEventsFile)
arrNewEventsFile=[]
for thisLine in arrEventsFile:
  thisLine=thisLine.rstrip() # Remove trailing whitespace, including line breaks
  thisLine=thisLine.replace("dc.b", "dc.w")
  thisLine=thisLine.replace("dc.w Events_", "dc.l Events_")
  arrNewEventsFile.append(thisLine)

Write(arrNewEventsFile, strEventsFile)

#-- This used to be conv_aks.bat
#------------------------------------------------------

strGeneratedFileInfoLine="; Used by sndh.s, generated by aks2sndh.py - so don't edit this file"

arrEventsFile=[]
arrInfoFile=[]
arrSongFile=[]


# Generate events file
arrEventsFile.append(strGeneratedFileInfoLine)
arrEventsFile.append("  .include \""+RmacSlash(strEventsFile)+"\"")

Write(arrEventsFile, "src"+os.sep+"_sndh_events.s")


# Generate song file
arrSongFile.append(strGeneratedFileInfoLine)
arrSongFile.append("  .include \""+RmacSlash(strSongFile)+"\"")

Write(arrSongFile, "src"+os.sep+"_sndh_song.s")


# Generate SNDH info file
arrInfoFile.append(strGeneratedFileInfoLine)
arrInfoFile.append("  dc.b \"SNDH\"")
if strSndhTitle:
  arrInfoFile.append("  dc.b \"TITL\",\""+strSndhTitle+"\",0")
if strSndhComposedBy:
  arrInfoFile.append("  dc.b \"COMM\",\""+strSndhComposedBy+"\",0")
if strSndhYear:
  arrInfoFile.append("  dc.b \"YEAR\",\""+strSndhYear+"\",0")
arrInfoFile.append("  dc.b \"TC"+str(numReplayFreq)+"\",0")
if numSndhTimeInSeconds:
  arrInfoFile.append("  dc.b \"TIME\"")
  arrInfoFile.append("  dc.w "+str(numSndhTimeInSeconds))
if strSndhRippedBy:
  arrInfoFile.append("  dc.b \"RIPP\",\""+strSndhRippedBy+"\",0")
if strSndhConvertedBy:
  arrInfoFile.append("  dc.b \"CONV\",\""+strSndhConvertedBy+"\",0")

arrInfoFile.append("  even")
arrInfoFile.append("  dc.b \"HDNS\"")
arrInfoFile.append("  even")

Write(arrInfoFile, "src"+os.sep+"_sndh_info.s")

#------------------------------------------------------
#-- This used to be build_sndh.bat


ExeWithOutput(["bin"+os.sep+"rmac", "-D_RMAC_=1", "-D_VASM_=0", "-fb", "src"+os.sep+"sndh.s"])
TestFile("src"+os.sep+"sndh.o")

ExeWithOutput(["bin"+os.sep+"rln", "-z", "-n", "-a", "0", "x", "x", "src"+os.sep+"sndh.o", "-o", strOutFile])
TestFile(strOutFile)


#-- This used to be build_sndh.bat
#------------------------------------------------------

if not boolQuietMode:
  print "Written file \""+strOutFile+"\" - all done!\n"

# Cleanup
KillFile("src"+os.sep+"sndh.o")
KillFile(strSongFile)
KillFile(strEventsFile)






